<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Portfolio\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => 'Shaun Sparg', 'email' => 'shaunsparg85@gmail.com', 'password' => Hash::make('ss198509')]);
    }
}
