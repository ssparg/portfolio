<?php

use Portfolio\Tag;
use Portfolio\Project;
use Illuminate\Support\Facades\Cache;

function returnIcon($name) {
	$icon = '';
	switch ($name) {
		case 'PHP':
			$icon = 'fab fa-php fa-2x';
			break;
		case 'JavaScript/Jquery':
			$icon = 'fab fa-js fa-2x';
			break;
		case 'MySQL':
			$icon = 'fas fa-database fa-2x';
			break;
		case 'Android':
			$icon = 'fab fa-android fa-2x';
			break;
		case 'CSS':
			$icon = 'fab fa-css3 fa-2x';
			break;
		case 'HTML':
			$icon = 'fab fa-html5 fa-2x';
			break;
		case 'vuejs':
			$icon = 'fab fa-vuejs fa-2x';
			break;
		case 'Wordpress':
			$icon = 'fab fa-wordpress fa-2x';
			break;
	}

	return $icon;
}

function getProjects()
{	Cache::forget('projects');
	$projects = Cache::rememberForever('projects', function() {
		return Project::select('id', 'name', 'url', 'image', 'image_small', 'image_medium', 'description')->with('tags')->orderBy('created_at', 'DESC')->get();
	});

	return $projects;
}

function getTags()
{
	$tags = Cache::rememberForever('tags', function() {
		return Tag::select('id', 'name')->where('parent','course')->get()->sortBy('name');
	});

	return $tags;
}