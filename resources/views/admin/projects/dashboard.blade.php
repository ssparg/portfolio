@extends('layouts.app')

@section('content')
<div class="container projects-dashboard">
	<div class="row mb-3">
		<div class="col">
			<a href="{{ route('admin.projects.create') }}" class="btn btn-outline-info btn-lg float-right">Add Project</a>
		</div>
	</div>
    <div class="row">
	@if ($projects->count())
        @foreach ($projects as $project)
        <div class="col-md-4">
            <div class="card shadow p-3 mb-5 bg-white rounded project" data-id="{{$project->id}}" data-href="{{ route('admin.projects.edit', $project->id) }}">
                <img src="{{ asset('storage/'.$project->image) }}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">{{ $project->name }}</h5>
                    <p class="card-text">
                        @if ($project->tags->count())
                        <ul>
                            @foreach ($project->tags as $tag)
                                <li class="{{returnIcon($tag->name)}}"></li>
                            @endforeach
                        </ul>
                        @endif
                    </p>
                </div>
            </div>
        </div>           
        @endforeach
    
    @else
        
    @endif
    </div>
</div>
@endsection
