<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123196440-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-123196440-1');
        </script>
        <meta name="Description" content="Hi! My name is Shaun Sparg, and I'm a PHP developer from Cape Town, South Africa">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link defer href="{{ asset('css/app.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/portfolio.css') }}" rel="stylesheet">
        {{--  <link href="{{ asset('css/print.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('css/media.min.css') }}" rel="stylesheet" media="(max-width: 1484px)">  --}}
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/favicons/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/favicons/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicons/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicons/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicons/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicons/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicons/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicons/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/favicons/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicons/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicons/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicons/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('images/favicons/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('images/favicons/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
        <title>{{ config('app.name') }}</title>
    </head>
    <body id="top">
        <header>
            <div class="header-container">
                <a href="#menu" class="box-shadow-menu"></a> <div class="intro">
                    <h1 class="text-center text-uppercase">Shaun Sparg</h1>
                    <h4 class="text-center">PHP Developer</h4>
                    <h6 class="text-center font-italic">Aspiring Mobile Developer</h6>
                </div>
                <nav>
                    <a class="close">X</a>
                    <ul class="nav-left">
                        <li><a href="#about">About</a></li>
                        <li><a href="#portfolio">Projects</a></li>
                        <li><a href="#skills">Skills</a></li>
                        <!-- <li><a href="#courses">Courses</a></li> -->
                    </ul>
                    <ul class="nav-right">                        
                        <li><a href="#experience">Experience</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>    
                    <div class="clear"></div>
                </nav>
            
            </div>
            <div class="modal"> </div>
         </header>
        <main>
            <div class="container">
                <section class="about" id="about">
                    <div class="row">
                        <div class="col">
                            <div class="portfolio-img">
                                <img src="{{ asset('images/me_200x200.jpg') }}" class="img-fluid mx-auto d-block" alt="Shaun Sparg">
                            </div>
                            
                        </div>    
                    </div>
                    <div class="row">
                        <div class="social">
                            <ul>
                                <li>
                                    <a style="color: #17a2b8" href="https://www.linkedin.com/in/ssparg/" target="_blank" rel="noopener"><i class="fab fa-linkedin fa-2x"></i></a>
                                </li>
                                <li>
                                    <a style="color: #17a2b8" href="https://bitbucket.org/ssparg/" target="_blank" rel="noopener"><i class="fab fa-bitbucket fa-2x"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col blob">
                            <!-- <p class="play"><span class="play-icon"><i class="fas fa-play fa-2x"></i></span></p> -->
                            <p>
                                I have worked on projects across multiple industries focusing primarly on the backend of things.  I, however, am not afraid to jump in and do stuff outside my expertese.
                                I believe there's more to a project then just building it, understanding requirements and your business is fundemental in delivering exactly what is needed.
                            </p>
                        </div>
                    </div>
                </section>  <!-- End of about -->
                <section class="portfolio" id="portfolio">
                    <h4>Projects</h4>
                    <span>I have plenty of projects from my career not listed here, contact me for a list.</span>
                    <a class="top-link" href="#top">top</a>
                    <div class="project-list">
                        @include('project-list')
                    </div>
                </section>
                <!-- <section class="courses" id="courses">
                    <h4>Courses</h4>
                    <a class="top-link" href="#top">top</a>
                        <ul>
                        @if ($courses->count())
                            @for ($i = 0; $i < $courses->count(); $i++)

                                <li><div class="circle"></div><div>{{ $courses[$i]->year }} : {{ $courses[$i]->name }}
                                    @isset($courses[$i]->url)
                                        <a href="{{ $courses[$i]->url }}" target="_blank" rel="noopener">details</a>
                                    @endisset
                                </div></li>
                            @endfor
                        @else
                            <li class="no-content">No courses have been added yet</li>
                        @endif  
                        </ul>
                </section> -->
                {{-- <section class="education">
                    <h4>Education</h4>
                </section> --}}
                <section class="skills" id="skills">
                    <h4>Skills</h4>
                    <a class="top-link" href="#top">top</a>
                    PHP Web Applications
                    <div class="progress" style="height: 30px;">
                      <div class="progress-bar progress-bar-striped bg-info php-indicator" role="progressbar" style="width: 70%;">7/10</div>
                    </div>
                    MySQL
                    <div class="progress" style="height: 30px;">
                      <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%;">5/10</div>
                    </div>
                    JavaScript
                    <div class="progress" style="height: 30px;">
                      <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%;">5/10</div>
                    </div>
                    Android Development
                    <div class="progress" style="height: 30px;">
                      <div class="progress-bar progress-bar-striped progress-bg bg-info" role="progressbar" style="width: 30%;">3/10</div>
                    </div>
                </section>
                <section class="experience" id="experience">
                    <h4>Experience</h4>
                    <a class="top-link" href="#top">top</a>
                    <div class="row">
                        <div class="col">
                            <div>Global Logistics S.A.</div><span>Oct 2019 - Current</span>
                            <ul>
                                <li>Developing features for existing solutions</li>
                                <li>Maintenance of existing solutions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div>Elemental Web Solutions</div><span>Mar 2016 - Oct 2019</span>
                            <ul>
                                <li>Development of Web Applications for clients</li>
                                <li>Restful API's</li>
                                <li>Database Design</li>
                                <li>Maintenance of existing solutions</li>
                                <li>Adding features to existing solutions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div>Regis Management</div><span>Dec 2012 - Feb 2016</span>
                            <ul>
                                <li>Development of Mobile Applications for clients</li>
                                <li>Development of Web Applications for clients</li>
                                <li>Restful API's</li>
                                <li>Research of possible business solutions</li>
                                <li>Weekly stats reporting</li>
                                <li>Database Creation, Queries, Stored Procedures</li>
                            </ul>
                        </div>
                    </div>
                </section>
                <section class="contact" id="contact">
                    <h4>Contact</h4>
                    <a class="top-link" href="#top">top</a>
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('front.contact') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" class="form-control" name="fullname" placeholder="Enter fullname">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="3" placeholder="Enter Message"></textarea>
                                </div>
                                <button type="button" class="btn float-right send-email bg-info">Send</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <footer>
            <div class="container-fluid">
                <p>&copy; {{ date('Y')}} shaunsparg.com</p>
            </div>
        </footer>
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script>
            function validateEmail(email) {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                    return true;
                }
                return false;
            }

            function countChars(a) {
                if (a.val().length == 0) {
                    a.addClass('error');
                } else {
                    if (a.attr('name') == 'email') {
                        if (!validateEmail(a.val())) {
                            a.addClass('error');
                        } else {
                            a.removeClass('error');
                        }
                    }
                    a.removeClass('error');
                }
            }

            function showLoader() {
                $('header .modal').html('<div class="lds-dual-ring"></div>');
                $('header .modal').show();
            }

            function hideLoader() {
                $('header .modal').html('');
                $('header .modal').hide();
            }


            $(document).ready(function(){
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                var name = $('input[name="fullname"]');
                var email = $('input[name="email"]');
                var msg = $('textarea[name="message"]');

                name.on('keyup', function(){
                    return countChars($(this));
                });

                email.on('keyup', function(){
                    return countChars($(this));
                });

                msg.on('keyup', function(){
                    return countChars($(this));
                });

                $('.send-email').on('click', function(e){
                    e.preventDefault();
                    var pass = true;
                    if (name.val() == '') {
                        name.addClass('error');
                        pass = false;
                    }
                    if (msg.val() == '') {
                        msg.addClass('error');
                        pass = false;
                    }
                    if (!validateEmail(email.val())) {
                        email.addClass('error');
                        pass = false;
                    }

                    if (pass) {
                        showLoader();
                        var url = $(this).closest('form').attr('action');
                        $.post({
                            url : url,
                            data: {
                                name: name.val(), 
                                email: email.val(),
                                msg: msg.val()
                            }
                        }).done(function(data){
                            if(data.success) {
                                hideLoader();
                                $('header .modal').html(data.html);
                                $('header .modal').show();
                                name.val('');
                                email.val('');
                                msg.val('');
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown){
                            hideLoader();
                        });
                    }
                });

                $('body').on('click', '.pagination a', function(e){
                    e.preventDefault();
                    var url = $(this).attr('href');
                    $.ajax({
                        url : url  
                    }).done(function(data) {
                        $('.project-list').fadeOut(function(){
                            $('.project-list').html(data);
                        });
                        $('.project-list').fadeIn();
                    }).fail(function() {
                        alert('Articles could not be loaded.');
                    });
                });

                $('.project-list').on('click', '.card', function(e){
                    e.stopPropagation();
                    showLoader();
                    var project_id = $(this).attr('data-id');
                    var url = "<?=url('projects')?>" + '/' + project_id;
                    $.ajax({
                        url:url
                    }).done(function(data){
                        hideLoader();
                        $('header .modal').html(data);
                        $('header .modal').show();
                        $('body').addClass('noscroll');
                    }).fail(function () {
                        hideLoader();
                        alert('Project could not be loaded.');
                    });
                });

                $('.modal').on('click', '.modal-close-btn', function(e){
                    e.stopPropagation();
                    $('header .modal').hide();
                    $('header .modal').empty();
                    $('body').removeClass('noscroll');
                });

                $('nav').on('click', 'li a', function(e){
                e.preventDefault();
                     $('nav').removeClass('open');            
                var hash = $(this).attr('href');  
                  $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                  }, 800, function(){
                     window.location.hash = hash;
                });
            });

            $('.top-link').on('click', function(e){
                e.preventDefault();           
                var hash = $(this).attr('href');  
                  $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                  }, 800, function(){
                     window.location.hash = hash;
                });
            });          

            $('.box-shadow-menu').on('click',function(e){
              if ($('nav').hasClass('open')) {
                $('nav').removeClass('open');
              } else {
                $('nav').addClass('open');              
             }
            });
            $('nav .close').on('click',function(e){
                if ($('nav').hasClass('open')) {
                    $('nav').removeClass('open');
                } else {
                    $('nav').addClass('open');              
                }
            });

            // var synth = window.speechSynthesis;
            // var utterThis = new SpeechSynthesisUtterance('hi there');;
            // function populateVoiceList() {
            //     voices = synth.getVoices();
            //     for (var i = 0; i < voices.length; i++) {
            //         if (voices[i].name === 'Microsoft David Desktop - English (United States)') {
            //             //utterThis.voice = voices[i];
            //         }
            //     }
            //     console.log(voices);
            // }

            // populateVoiceList();
            // if (speechSynthesis.onvoiceschanged !== undefined) {
            //     speechSynthesis.onvoiceschanged = populateVoiceList;
            // }

            // $('.play-icon').on('click', function() {
            //     if (synth.speaking) {
            //         return;
            //     }
                
            //     utterThis.pitch = 1;
            //     utterThis.rate = 1;
            //     synth.speak(utterThis);
            // });
        });
        </script>
    </body>
</html>
